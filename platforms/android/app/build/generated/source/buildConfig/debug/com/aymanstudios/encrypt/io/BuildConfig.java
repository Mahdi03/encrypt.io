/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.aymanstudios.encrypt.io;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.aymanstudios.encrypt.io";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 10100;
  public static final String VERSION_NAME = "1.1.0";
}
